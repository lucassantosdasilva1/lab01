import edu.cest.lab01.Pessoa;
import edu.cest.lab01.Animal;
import edu.cest.lab01.Cliente;
public class Aplicacao{
	public static void main(String[] args){
		Pessoa pess1 = new Pessoa();
		pess1.nome = "Fulano";
		Animal ani1 = new Animal();
		ani1.nome = "Spyke";
		Cliente cli1 = new Cliente();
		cli1.nome = "Ciclano";
		System.out.println("Pessoa Nome: " + pess1.nome);
		System.out.println("Animal Nome: " + ani1.nome);
		System.out.println("Cliente Nome: " + cli1.nome);
		}
}
